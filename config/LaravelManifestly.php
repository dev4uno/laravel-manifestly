<?php

return [
    'api_key' => env('MANIFESTLY_API_KEY'),
    'url' => env('MANIFESTLY_URL', 'https://api.manifest.ly/api/v1'),
    'mock_server_enable' => env('MANIFESTLY_MOCK_SERVER_ENABLE', false),
];
