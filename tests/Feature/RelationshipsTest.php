<?php
declare(strict_types=1);

namespace Bamba\LaravelManifestly\Tests\Feature;

use Bamba\LaravelManifestly\Proxies\ProxyOfCollection;
use Bamba\LaravelManifestly\Resources\Checklist;
use Bamba\LaravelManifestly\Resources\ChecklistStep;
use Bamba\LaravelManifestly\Resources\ChecklistStepComment;
use Bamba\LaravelManifestly\Resources\Workflow;
use Bamba\LaravelManifestly\Tests\TestCase;
use Illuminate\Support\Collection;

class RelationshipsTest extends TestCase
{
    public function testRelationships()
    {
        $workflows = app(Workflow::class);
        $workflow = $workflows->create([
            'business_days' => [
            ],
            'description' => 'We use this when we hire a new employee',
            'external_id' => 'some-external-identifier-003',
            'hide_steps_from_external' => false,
            "expected_duration" => 30,
            'expected_duration_units' => 'days',
            'tag_list' => 'hr',
            'title' => 'New Employee Y',
            'steps' => [
                [
                    'title' => 'Send policy',
                    'header_step' => true,
                ],
                [
                    'title' => 'Whats app message',
                    'header_step' => false,
                ],
            ],
        ]);

        $checklist = app(Checklist::class);
        $checklist = $checklist->create([
            'checklist_id' => $workflow->id,
            'title' => 'Testing the creation',
            'tag_list' => ['testing', 'relationships'],
            'external_id' => 'https://vivebamba.com',
        ]);
        $this->assertInstanceOf(Collection::class, $checklist->steps);
        $this->assertInstanceOf(ChecklistStep::class, $checklist->steps->first());
        $this->assertInstanceOf(ProxyOfCollection::class, $checklist->steps());
        $this->assertInstanceOf(Collection::class, $checklist->steps()->get());
        /** @var ChecklistStep $step */
        $step = $checklist->steps->get(1);
        $this->assertInstanceOf(Collection::class, $step->comments);

        $comment = $step->comments()->create(['comment' => 'Creation of commentary']);
        $this->assertInstanceOf(ChecklistStepComment::class, $comment);
    }
}
