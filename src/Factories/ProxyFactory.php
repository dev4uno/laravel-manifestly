<?php
declare(strict_types=1);

namespace Bamba\LaravelManifestly\Factories;

class ProxyFactory
{
    /**
     * @template T
     * @param class-string<T> $className
     * @return T
     */
    public function create(string $className)
    {
        return app($className);
    }
}
