<?php
declare(strict_types=1);

namespace Bamba\LaravelManifestly\Proxies;

use Bamba\LaravelManifestly\Factories\ProxyFactory;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

abstract class Proxy
{
    /**
     * @var Client
     */
    protected $client;
    protected $resource;
    protected $path;
    protected $className;
    /**
     * @var ProxyFactory
     */
    protected $proxyFactory;

    public function __construct(Client $client, ProxyFactory $proxyFactory)
    {
        $this->client = $client;
        $this->proxyFactory = $proxyFactory;
    }

    abstract public function get();

    public function create(array $data)
    {
        $response = $this->client->post($this->path.'/'.$this->className::getCollectionName(), [
            RequestOptions::JSON => $data,
        ]);
        $content = json_decode($response->getBody()->getContents(), true);

        $resource = new $this->className($this->proxyFactory);

        if (!empty($content)) {
            $resource->fill($content[$this->className::getSingleName()]);
        }

        return $resource;
    }

    /**
     * @param string|ProxyOfCollection $className
     * @return $this
     */
    public function path($path)
    {
        if ($path === null) {
            return $this->path;
        }

        $this->path = $path;

        return $this;
    }

    /**
     * @param string|ProxyOfCollection $className
     * @return $this
     */
    public function className(string $className = null)
    {
        if ($className === null) {
            return $this->className;
        }

        $this->className = $className;

        return $this;
    }
}
