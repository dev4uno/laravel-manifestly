<?php
declare(strict_types=1);

namespace Bamba\LaravelManifestly\Proxies;

use Bamba\LaravelManifestly\Resources\Resource;
use Illuminate\Support\Collection;

class ProxyOfCollection extends Proxy
{
    public function get()
    {
        $response = $this->client->get($this->path.'/'.$this->className::getCollectionName());
        $content = json_decode($response->getBody()->getContents(), true);

        if (!key_exists($this->className::getCollectionName(), $content)) {
            return collect();
        }

        $collection = collect();
        foreach ($content[$this->className::getCollectionName()] as $resourceData) {
            $resource = new $this->className($this->proxyFactory);

            // Some relationship are loaded in one requests, thus transform that response
            $this->loadNestedResources($resource, $resourceData);

            $resource->fill($resourceData);
            $resource->parentPath = $this->path;

            $collection->push($resource);
        }

        return $collection;
    }

    /**
     * @template T
     * @param array $resourceData
     * @param class-string<T> $nestedResourceName
     * @param Resource $currentResource
     * @return Collection|T
     */
    private function createNestedResource(array $resourceData, string $nestedResourceName, Resource $currentResource)
    {
        if (is_array($resourceData[$nestedResourceName])) {
            $nestedResource = new Collection();
            foreach ($resourceData[$nestedResourceName] as $nestedResourceData) {
                $nestedResourceClass = $currentResource->$nestedResourceName()->className();
                $nestedResourceInstance = new $nestedResourceClass($this->proxyFactory);
                $nestedResourceInstance->fill($nestedResourceData);
                $nestedResource->push($nestedResourceInstance);
            }

            return $nestedResource;
        }
        $nestedResourceClass = $currentResource->$nestedResourceName()->className();
        $nestedResource = new $nestedResourceClass($this->proxyFactory);
        $nestedResource->fill($resourceData[$nestedResourceName]);


        return $nestedResource;
    }

    private function loadNestedResources(Resource $resource, array $resourceData)
    {
        /** @var Resource $currentInstance */
        $currentInstance = new $this->className($this->proxyFactory);
        foreach ($currentInstance->width as $nestedResourceName) {
            // Call the relationship to retrieve the class name of the relationship.
            if (array_key_exists($nestedResourceName, $resourceData)) {
                $nestedResource = $this->createNestedResource(
                    $resourceData,
                    $nestedResourceName,
                    $currentInstance);
                $resource->nestedResources[$nestedResourceName] = $nestedResource;
                unset($resourceData[$nestedResourceName]);
            }
        }
    }
}
