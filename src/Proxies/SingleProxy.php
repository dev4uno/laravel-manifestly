<?php
declare(strict_types=1);

namespace Bamba\LaravelManifestly\Proxies;

class SingleProxy extends Proxy
{
    public function get()
    {
        $response = $this->client->get($this->path);
        $content = json_decode($response->getBody()->getContents(), false);

        $resource = new $this->className($this->client);
        $resource->fill($content);
        $resource->parentPath = $this->path;

        return $resource;
    }
}
