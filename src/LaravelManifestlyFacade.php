<?php

namespace Bamba\LaravelManifestly;

use Illuminate\Support\Facades\Facade;

class LaravelManifestlyFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'laravel-manifestly';
    }
}
