<?php
declare(strict_types=1);

namespace Bamba\LaravelManifestly\Resources;

use Bamba\LaravelManifestly\Proxies\ProxyOfCollection;
use Illuminate\Support\Collection;

/**
 * Class Checklist
 *
 * @package Bamba\LaravelManifestly\Resources
 *
 * @property int $id
 * @property Collection $steps
 */
class Checklist extends Resource
{
    private const COLLECTION_NAME = 'runs';
    private const SINGLE_NAME = 'run';

    public static function getSingleName(): string
    {
        return self::SINGLE_NAME;
    }

    public static function getCollectionName(): string
    {
        return self::COLLECTION_NAME;
    }

    public function steps(): ProxyOfCollection
    {
        $path = self::BASE_PATH.'/'.Checklist::COLLECTION_NAME.'/'.$this->id;

        return $this->proxyOfCollection($path, ChecklistStep::class);
    }
}
