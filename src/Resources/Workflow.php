<?php
declare(strict_types=1);

namespace Bamba\LaravelManifestly\Resources;

/**
 * Class Workflow
 *
 * @package Bamba\LaravelManifestly\Resources
 *
 * @property int $id
 */
class Workflow extends Resource
{

    public static function getSingleName(): string
    {
        return 'checklist';
    }

    public static function getCollectionName(): string
    {
        return 'checklists';
    }
}
