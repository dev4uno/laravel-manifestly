<?php
declare(strict_types=1);

namespace Bamba\LaravelManifestly\Resources;

class ChecklistStepData extends Resource
{
    public static function getSingleName(): string
    {
        return 'data';
    }

    public static function getCollectionName(): string
    {
        return 'data';
    }
}
