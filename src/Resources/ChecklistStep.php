<?php
declare(strict_types=1);

namespace Bamba\LaravelManifestly\Resources;

use Bamba\LaravelManifestly\Proxies\ProxyOfCollection;
use Bamba\LaravelManifestly\Proxies\SingleProxy;
use Illuminate\Support\Collection;

/**
 * Class ChecklistStep
 *
 * @package Bamba\LaravelManifestly\Resources
 * @property int $id
 * @property Collection $comments
 * @property ChecklistStepData $data
 * @property int $position
 */
class ChecklistStep extends Resource
{
    const COLLECTION_NAME = 'run_steps';
    const SINGLE_NAME = 'run_step';

    public $width = [
        'comments',
    ];

    public static function getSingleName(): string
    {
        return self::SINGLE_NAME;
    }

    public static function getCollectionName(): string
    {
        return self::COLLECTION_NAME;
    }

    /**
     * Warning: If you create a data then the step is completed.
     *
     * @return SingleProxy
     */
    public function data(): SingleProxy
    {
        $parentPath = $this->parentPath.'/'.self::COLLECTION_NAME.'/'.$this->id;

        return $this->proxy($parentPath, ChecklistStepData::class);
    }

    public function comments(): ProxyOfCollection
    {
        $parentPath = $this->parentPath.'/'.self::COLLECTION_NAME.'/'.$this->id;

        return $this->proxyOfCollection($parentPath, ChecklistStepComment::class);
    }
}
