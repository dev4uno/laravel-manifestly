<?php
declare(strict_types=1);

namespace Bamba\LaravelManifestly\Resources;

/**
 *
 * @property int $id
 */
class ChecklistStepComment extends Resource
{
    const COLLECTION_NAME = 'comments';
    const SINGLE_NAME = 'comment';

    public static function getSingleName(): string
    {
        return self::SINGLE_NAME;
    }

    public static function getCollectionName(): string
    {
        return self::COLLECTION_NAME;
    }
}
