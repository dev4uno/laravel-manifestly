<?php
declare(strict_types=1);

namespace Bamba\LaravelManifestly\Resources;

use Bamba\LaravelManifestly\Factories\ProxyFactory;
use Bamba\LaravelManifestly\Proxies\ProxyOfCollection;
use Bamba\LaravelManifestly\Proxies\SingleProxy;
use Illuminate\Support\Collection;
use LogicException;

abstract class Resource
{
    const BASE_PATH = '/api/v1';

    public $parentPath = '';

    public $width = [];

    protected $attributes = [];

    /**
     * @var Collection[]
     */
    public $nestedResources = [];

    /**
     * @var ProxyFactory
     */
    protected $proxyFactory;

    public function __construct(ProxyFactory $proxyFactory)
    {
        $this->proxyFactory = $proxyFactory;
    }

    abstract public static function getSingleName(): string;

    abstract public static function getCollectionName(): string;

    /**
     * @param $payload
     * @return static
     */
    public function create($payload)
    {
        $path = self::BASE_PATH;

        $proxy = $this->proxyFactory->create(ProxyOfCollection::class);
        $proxy->path($path);
        $proxy->className(static::class);

        $checklist = $proxy->create($payload);

        return $checklist;
    }

    public function fill(array $data)
    {
        $this->attributes = array_merge($this->attributes, $data);
    }

    /**
     * @param string $key snake case to properties and camelCase to resources (also can be called relationships)
     * @return static|Collection|null
     */
    public function __get(string $key)
    {
        if ($key === 'get') {
            $this->$key();
        }

        if (array_key_exists($key, $this->attributes)) {
            return $this->attributes[$key];
        }

        // Check if the relationship was loaded. If it was loaded then return it.
        if (array_key_exists($key, $this->nestedResources)) {
            return $this->nestedResources[$key];
        }

        if (method_exists($this, $key)) {
            $proxy = $this->$key();
            if (!$proxy instanceof ProxyOfCollection) {
                throw new LogicException(
                    'The method '.$key.' have to return and '.ProxyOfCollection::class.' instance.'
                );
            }

            $this->nestedResources[$key] = $proxy->get();

            return $this->nestedResources[$key];
        }

        return null;
    }

    protected function proxyOfCollection($path, $className): ProxyOfCollection
    {
        $proxy = $this->proxyFactory->create(ProxyOfCollection::class);
        $proxy->path($path);
        $proxy->className($className);

        return $proxy;
    }

    protected function proxy($path, $className)
    {
        $proxy = $this->proxyFactory->create(SingleProxy::class);
        $proxy->path($path);
        $proxy->className($className);

        return $proxy;
    }

}
