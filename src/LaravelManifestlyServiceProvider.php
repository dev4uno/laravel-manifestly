<?php

namespace Bamba\LaravelManifestly;

use Bamba\LaravelManifestly\MockClients\MockClient;
use Bamba\LaravelManifestly\Proxies\ProxyOfCollection;
use Bamba\LaravelManifestly\Proxies\SingleProxy;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;
use Illuminate\Support\ServiceProvider;

class LaravelManifestlyServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/LaravelManifestly.php', 'laravel-manifestly');

        $this->publishConfig();
    }


    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register facade
        $this->app->singleton('laravel-manifestly', function () {
            return new LaravelManifestly;
        });

        $this->app->singleton('manifestly.http-client', function () {
            $options = [
                'headers' => ['Content-Type' => 'application/json'],
                'base_uri' => config('laravel-manifestly.url'),
                RequestOptions::QUERY => [
                    'api_key' => config('laravel-manifestly.api_key'),
                ],
            ];
            if (!config('laravel-manifestly.mock_server_enable')) {
                return new Client($options);
            }

            return new MockClient($options);
        });

        $this->app->when(ProxyOfCollection::class)
            ->needs(Client::class)
            ->give('manifestly.http-client');

        $this->app->when(SingleProxy::class)
            ->needs(Client::class)
            ->give('manifestly.http-client');
    }

    /**
     * Publish Config
     *
     * @return void
     */
    public function publishConfig()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__.'/../config/LaravelManifestly.php' => config_path('LaravelManifestly.php'),
            ], 'config');
        }
    }
}
