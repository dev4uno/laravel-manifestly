<?php
declare(strict_types=1);

namespace Bamba\LaravelManifestly\MockClients;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class MockClient extends Client
{
    /**
     * @var MockHandler
     */
    private $mock;

    public function __construct(array $config = [])
    {
        $this->mock = new MockHandler();
        $config['handler'] = $this->mock;
        parent::__construct($config);
    }

    /**
     * @param string $method
     * @param string $uri
     * @param array $options
     * @return ResponseInterface
     * @throws GuzzleException
     */
    public function request($method, $uri = '', array $options = []): ResponseInterface
    {
        $this->mock->reset();

        $pathFile = __DIR__.'/../../resources/mock-server-responses'.$uri.'/'.strtolower($method).'.json';
        $content = file_get_contents($pathFile);
        $this->mock->append(
            new Response(
                200,
                [],
                $content
            )
        );

        return parent::request($method, $uri, $options);
    }
}
